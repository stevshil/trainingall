# Simple PHP example

This app should be compiled into a Docker image stream on OpenShift.

To build the container do the following;

* oc new-app https://bitbucket.org/stevshil/trainingall --context-dir=php

OpenShift will default to a PHP container
